package com.piwriw.calculateapp.provider;

import com.piwriw.calculateapp.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.*;
import ohos.distributedschedule.interwork.DeviceInfo;

import java.util.List;

/**
 * @author Piwriw
 * @date 2021/11/16 18:58
 */
public class DeviceProvider extends BaseItemProvider {
    List<DeviceInfo> list;
    AbilitySlice slice;

    public DeviceProvider(List<DeviceInfo> list, AbilitySlice slice) {
        this.list = list;
        this.slice = slice;
    }

    @Override
    public int getCount() {
        if (list!=null){
            return list.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        if(list!=null){
            return list.get(i);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    //把数据根据i放到item_device
    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        Component cpt = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_item_device,null,false);
        Text nameText = (Text)cpt.findComponentById(ResourceTable.Id_item_device_name);
        nameText.setText(list.get(i).getDeviceName());
        return cpt;
    }
}
