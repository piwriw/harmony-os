package com.piwriw.calculateapp.bean;

/**
 * @author Piwriw
 * @date 2021/11/15 21:43
 */
public class CalculationFormula {
        Integer a;
        Integer b;
        Integer result;
        String flag;
    public CalculationFormula() {
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public CalculationFormula(Integer a, Integer b, Integer result, String flag) {
        this.a = a;
        this.b = b;
        this.result = result;
        this.flag = flag;
    }

    public Integer getA() {
        return a;
    }

    public void setA(Integer a) {
        this.a = a;
    }

    public Integer getB() {
        return b;
    }

    public void setB(Integer b) {
        this.b = b;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

}
