package com.piwriw.calculateapp.utils;

import ohos.aafwk.ability.AbilitySlice;
import ohos.global.resource.Resource;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;

public class Utils {
    public static void logInfo(String info){
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP,0,"MyLogTest");
        String format = "%{public}s";
        HiLog.info(logLabel,format,info);
    }

    public static PixelMap getPixelMapFromPath(String imageUrl, AbilitySlice slice){
        try {
            //从路径中读取出了图片文件的数据
            Resource resource = slice.getResourceManager().getRawFileEntry(imageUrl).openRawFile();
            //对图片的格式进行说明,我们采用的是标准文件，无需特殊说明
            ImageSource.SourceOptions sourceOptions = new ImageSource.SourceOptions();
            //创建图片资源对象
            ImageSource imageSource = ImageSource.create(resource,sourceOptions);
            //需要对输出的图片进行说明，同样我们输出的是标准格式，无需特殊说明
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            //输出一张图片,创建一个PixelMap格式的图片数据
            return imageSource.createPixelmap(decodingOptions);
        }catch (IOException e){
            e.printStackTrace();
        }

        return null;
    }
}
