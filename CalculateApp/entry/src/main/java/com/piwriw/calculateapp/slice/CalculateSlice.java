package com.piwriw.calculateapp.slice;


import com.babushkatext.BabushkaText;
import com.piwriw.calculateapp.ResourceTable;
import com.piwriw.calculateapp.bean.CalculationFormula;
import com.piwriw.calculateapp.provider.DeviceProvider;
import com.piwriw.calculateapp.utils.Utils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.IAbilityContinuation;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.CommonDialog;
import ohos.distributedschedule.interwork.DeviceInfo;
import ohos.distributedschedule.interwork.DeviceManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Piwriw
 * @date 2021/11/15 21:40
 */
public class CalculateSlice extends AbilitySlice implements IAbilityContinuation {
    private int a;
    private int b;
    private int result;
    private String flag;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_calculate_main);
        initContainer();
        Button transButton = (Button) findComponentById(ResourceTable.Id_calculate_trans);
        transButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                requestPermissions();
                //系统对话框
                CommonDialog commonDialog = new CommonDialog(CalculateSlice.this);
                Utils.logInfo("trans 点击");
                //设备listcontainer
                ListContainer deviceListContainer = new ListContainer(CalculateSlice.this);
                //获取在线设备的集合
                List<DeviceInfo> deviceInfoList = DeviceManager.getDeviceList(DeviceInfo.FLAG_GET_ONLINE_DEVICE);
                DeviceProvider deviceProvider = new DeviceProvider(deviceInfoList, CalculateSlice.this);
                deviceListContainer.setItemProvider(deviceProvider);
                deviceListContainer.setItemClickedListener((listContainer, component1, position, id) -> {
                    DeviceInfo deviceInfo = deviceInfoList.get(position);
                    Utils.logInfo("设备名：" + deviceInfo.getDeviceName());
                    Utils.logInfo("设备id：" + deviceInfo.getDeviceId());
                    //窗口取消
                    commonDialog.destroy();
                        continueAbility(deviceInfo.getDeviceId());
                });
                //设置大小
                commonDialog.setSize(1000, 800);
                //设置标题
                commonDialog.setTitleText("设备列表");
                //放入listcontainer
                commonDialog.setContentCustomComponent(deviceListContainer);
                //显示
                commonDialog.show();

            }
        });

    }
    private void requestPermissions() {
        String[] permission = {
                "ohos.permission.DISTRIBUTED_DATASYNC",
                "ohos.permission.GET_DISTRIBUTED_DEVICE_INFO"};
        List<String> applyPermissions = new ArrayList<>();
        for (String element : permission) {
            if (verifySelfPermission(element) != 0) {
                if (canRequestPermission(element)) {
                    applyPermissions.add(element);
                } else {
                }
            } else {
            }
        }
        requestPermissionsFromUser(applyPermissions.toArray(new String[0]), 0);
    }

    void initContainer() {
        CalculationFormula formula = new CalculationFormula();
        Random random = new Random();
        Integer A = random.nextInt(100);
        Integer B = random.nextInt(100);
        formula.setA(A);
        formula.setB(B);
        a=A;
        b=B;
        String symbol[] = {"-", "+"};
        int flagNum = random.nextInt(1);
        formula.setFlag(symbol[flagNum]);
        flag=symbol[flagNum];
        {
            if (flagNum == 0) {
                formula.setResult(A - B);
                result=a-b;
            } else {
                formula.setResult(A + B);
                result=a+b;
            }
        }
        BabushkaText babushka = (BabushkaText)findComponentById(ResourceTable.Id_calculate_main_baText);
        babushka.addPiece(new BabushkaText.Piece.Builder(String.valueOf(A))
                .textColor(Color.getIntColor("#0081E2"))
                .textSizeRelative(2f)
                .build());
        babushka.addPiece(new BabushkaText.Piece.Builder(symbol[flagNum])
                .textColor(Color.getIntColor("#969696"))
                .textSizeRelative(2f)
                .build());
        babushka.addPiece(new BabushkaText.Piece.Builder(String.valueOf(B) )
                .textColor(Color.getIntColor("#0081E2"))
                .textSizeRelative(2f)
                .build());
        babushka.addPiece(new BabushkaText.Piece.Builder("=")
                .textColor(Color.getIntColor("#969696"))
                .textSizeRelative(2f)
                .build());
        babushka.display();
        TextField textFieldResult = (TextField) findComponentById(ResourceTable.Id_calculate_result);
        Button buttonMain = (Button) findComponentById(ResourceTable.Id_calculate_put);
        buttonMain.setText("提交");
        buttonMain.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Utils.logInfo(String.valueOf(formula.getResult()));
                Intent intent = new Intent();
                String ok;
                if (textFieldResult.getText().equals(String.valueOf(formula.getResult()))) {
                    ok = "恭喜你，答对了，你太棒了！<（￣︶￣）>";
                    Utils.logInfo(ok);
                } else {
                    ok = "对不起，你回答错误了!再看一下下吧 ╮（￣﹏￣）╭";
                    Utils.logInfo(ok);
                }
                intent.setParam("A", formula.getA());
                intent.setParam("B", formula.getB());
                intent.setParam("result", formula.getResult());
                intent.setParam("flag", formula.getFlag());
                intent.setParam("ok", ok);
                //这里我们开始启动了一个新的ResultSlice
                present(new ResultSlice(), intent);
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
    @Override
    public boolean onStartContinuation() {
        Utils.logInfo("开始连接");
        return true;
    }
    @Override
    public boolean onSaveData(IntentParams intentParams) {
//        intentParams.setParam("A", a);
//        intentParams.setParam("B", b);
//        intentParams.setParam("result", result);
//        intentParams.setParam("flag", flag);
//        Utils.logInfo(flag);
        return true;
    }
    @Override
    public boolean onRestoreData(IntentParams intentParams) {
//        a = (int) intentParams.getParam("A");
//        b = (int) intentParams.getParam("B");
//        result = (int) intentParams.getParam("result");
//        flag = (String) intentParams.getParam("flag");
        return true;
    }
    @Override
    public void onCompleteContinuation(int i) {
        terminate();
    }
}