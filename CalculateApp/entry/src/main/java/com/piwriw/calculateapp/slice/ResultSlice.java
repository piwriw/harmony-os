package com.piwriw.calculateapp.slice;

import com.piwriw.calculateapp.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

/**
 * @author Piwriw
 * @date 2021/11/16 21:22
 */
public class ResultSlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_calculate_result);
        int a=intent.getIntParam("A",0);
        int b=intent.getIntParam("B",0);
        int result=intent.getIntParam("result",0);
        String flag = intent.getStringParam("flag");
        String ok = intent.getStringParam("ok");
        Text mainText=(Text)findComponentById(ResourceTable.Id_calculate_result_main);
        mainText.setText(String.valueOf(a)+flag+ String.valueOf(b)+"="+result+ok);
        Button nextCalButton=(Button)findComponentById(ResourceTable.Id_calculate_nextCal);
        nextCalButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent1 = new Intent();
                Operation operation=new Intent.OperationBuilder()
                        .withAction("action.intent.CalculateSlice")
                        .build();
                intent1.setOperation(operation);
                startAbility(intent1);
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}